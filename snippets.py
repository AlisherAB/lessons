# магический метод для ввода данных

# a, b, c, x, v, g = map(int, input().split())
# print(f'a = {a}')
# print(f'b = {b}')
# print(f'b = {b}')
#
#
# my_list = []
# my_list.append(1)  # добавить список
#
# print(my_list)
# word = 'qwertyuiop'
# print(len(word))
# print(word[1:4])
# b = word[1:4]
# print(b)

# print(word[1:9:3])
# my_tuple = (23, 24)
# my_list = [2, 'hello', 'hello world', 520]
# print(my_list)
# spisok = list("555gfdjhgk")
# a = int(spisok[0])
# b = int(spisok[1])
# print(a+b)
# print(spisok)

# my_list = [1, 2, 3, 4, 5]
# my_list_2 = [7, 8, 9, 10]
# print(my_list)
# print(my_list_2)
# my_list.append(6)
# print(my_list)
# my_list.extend(my_list_2)
# print(my_list)
# a = my_list[0:9:2]
# print(a)
# my_list = [4,7,5,6,1,0,9,3,2,0,9,5,6,4,8,7,3,2,6,7,6,2,8,5,7,8,9]
# my_list.sort()
# print(my_list)
#
# my_list[1] = 1
# print(my_list)
# my_tuple = (0, 0, 1, 2, 2, 2, 3, 9)
# print(my_tuple)
# # my_tuple[1] = 1
# # print(my_tuple)
# my_tuple_1 = list(my_tuple)
# print(my_tuple_1)
# my_tuple_1[1] = 1
# print(my_tuple_1)
# my_tuple = tuple(my_tuple_1)
# print(my_tuple)
# my_dict = {'hello': 1, 'world': 2, 'word':5, 12:'20'}
# print(my_dict)
# print(my_dict['hello'])
# a = dict(((1,(1,2)), (2,2), (3,3)))
# print(a)
# my_dict['hello'] = 'world'
# print(my_dict)
# print(my_dict.keys())